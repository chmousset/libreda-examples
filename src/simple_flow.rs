/*
 * Copyright (c) 2020-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// stdlib
use std::fs;
use std::collections::{HashMap, BTreeSet, HashSet};
use std::io::{BufReader, BufWriter};
use std::borrow::Borrow;
use std::process::exit;

// crates.io
use log::*;
use itertools::Itertools;

use libreda_db::prelude as db;
use libreda_db::prelude::*;

use libreda_oasis::*;
use libreda_structural_verilog::*;

use libreda_pnr::util::netlist_validation;

use libreda_db::layout::types::UInt;
use libreda_lefdef::lef_ast::Layer;
use libreda_pnr::rebuffer::buffer_insertion::SimpleBufferInsertion;
use libreda_lefdef::{LEF, LefDefParseError};
use std::ffi::OsStr;
use mycelium_router::power_router::*;
use libreda_pnr::place::stdcell_placer::PlacerAdapter;
use electron_placer::SimpleQuadraticPlacer;
use electron_placer::eplace_ms::EPlaceMS;
use libreda_pnr::place::mixed_size_placer_cascade::PlacerCascade;
use libreda_pnr::place::placement_problem::PlacementStatus;
use libreda_pnr::design::SimpleDesignRef;
use libreda_pnr::place::mixed_size_placer::MixedSizePlacer;

use libreda_pnr::route::prelude::*;

use mycelium_router::maze_router::{SimpleMazeRouter, SimpleGlobalRouter, SimpleGlobalRoute};
use libreda_lefdef::def_ast::Tracks;


/// Bundle of layout, netlist, technology data,
/// macro dimension, etc.
pub struct SimpleFlow<C: db::L2NEdit> {
    /// Layout and netlist data.
    pub chip: C,
    /// Path to technology LEF file.
    pub tech_lef_path: std::path::PathBuf,
    /// Technology LEF data.
    pub tech_lef: LEF,
    pub top_cell: Option<C::CellId>,
    /// Names of clock nets in the top cell.
    pub clock_nets: Vec<String>,
    /// Names of reset nets in the top cell.
    pub reset_nets: Vec<String>,
    /// The shape of the chip/macro.
    pub core_area: Option<db::SimplePolygon<C::Coord>>,
    /// The layer to be used to for cell outlines (abutment boxes).
    pub outline_layer: Option<C::LayerId>,
}

impl<C: db::L2NEdit<Coord=db::Coord>> SimpleFlow<C>
    where C::NetId: Sync {
    pub fn new() -> Self {
        let mut simple_flow = Self {
            chip: C::new(),
            tech_lef_path: Default::default(),
            tech_lef: Default::default(),
            top_cell: Default::default(),
            clock_nets: Default::default(),
            reset_nets: Default::default(),
            core_area: Default::default(),
            outline_layer: Default::default(),
        };

        simple_flow.init();

        simple_flow
    }

    fn init(&mut self) {
        self.chip.set_dbu(1000);
        self.initialize_layers();
    }

    /// Get a read-only reference to the top cell.
    /// Panics if no top-cell is defined.
    fn top_cell_ref(&self) -> CellRef<C> {
        self.chip.cell_ref(
            self.top_cell.as_ref().expect("no top-cell defined")
        )
    }

    /// Register the name of a clock net.
    pub fn add_clock_net(&mut self, name: String) {
        self.clock_nets.push(name);
    }

    /// Register the name of a reset net.
    pub fn add_reset_net(&mut self, name: String) {
        self.reset_nets.push(name);
    }

    /// Read the technology LEF file.
    pub fn read_technology_lef(&mut self, path: &str) -> Result<(), LefDefParseError> {
        info!("Read technology LEF: {}", path);
        self.tech_lef_path = path.into();
        let mut lef_file = BufReader::new(fs::File::open(path).unwrap());
        let lef_result = libreda_lefdef::lef_parser::read_lef_bytes(&mut lef_file);
        if let Err(err) = &lef_result {
            error!("Failed to read LEF: {}", err)
        }
        self.tech_lef = lef_result?;

        Ok(())
    }

    pub fn report_routing_stack(&self) {
        {
            // Print statistics.
            let technology_lef = &self.tech_lef.technology;
            log::info!("Number of routing layers: {}", technology_lef.layers.iter().filter(|l| match l {
                Layer::Routing(_) => true,
                _ => false
            }).count());
            log::info!("Number of via layers: {}", technology_lef.layers.iter().filter(|l| match l {
                Layer::Cut(_) => true,
                _ => false
            }).count());
        }

        {
            // Get names of routing layer from the LEF file.
            let layer_stack_names = self.get_layer_stack_names();
            log::info!("Routing layer stack: {}", layer_stack_names.iter().join(", "));
        }
    }

    /// Get names of routing layers, ordered from lowest (close to silicon) to top.
    pub fn get_routing_layer_names(&self) -> Vec<String> {
        self.tech_lef.technology.layers.iter()
            .filter_map(|layer| {
                match layer {
                    Layer::Routing(l) => Some(l.name.clone().into()),
                    _ => None,
                }
            })
            .collect()
    }

    /// Get names of via layers, ordered from lowest (close to silicon) to top.
    pub fn get_via_layer_names(&self) -> Vec<String> {
        self.tech_lef.technology.layers.iter()
            .filter_map(|layer| {
                match layer {
                    Layer::Cut(l) => Some(l.name.clone().into()),
                    _ => None,
                }
            })
            .collect()
    }

    /// Get names of routing layer from the LEF file.
    pub fn get_layer_stack_names(&self) -> Vec<String> {
        self.tech_lef.technology.layers.iter()
            .filter_map(|layer| {
                match layer {
                    Layer::Routing(l) => Some(l.name.clone().into()),
                    Layer::Cut(l) => Some(l.name.clone().into()),
                    _ => None, // Ignore other layers than metal and via.
                }
            })
            .collect()
    }

    /// Get IDs of routing layers, ordered from lowest (close to silicon) to top.
    pub fn get_routing_layers(&mut self) -> Vec<C::LayerId> {
        // Create a list of layer IDs of the routing layers.
        let layer_name2index = self.get_layer_name_mapping();
        self.get_routing_layer_names().iter()
            .filter_map(|name| layer_name2index.get(name).copied())
            .map(|(idx, dtype)| self.chip.find_or_create_layer(idx, dtype))
            .collect()
    }

    /// Get IDs of via layers, ordered from lowest (close to silicon) to top.
    pub fn get_via_layers(&mut self) -> Vec<C::LayerId> {
        // Create a list of layer IDs of the routing layers.
        let layer_name2index = self.get_layer_name_mapping();
        self.get_via_layer_names().iter()
            .filter_map(|name| layer_name2index.get(name).copied())
            .map(|(idx, dtype)| self.chip.find_or_create_layer(idx, dtype))
            .collect()
    }

    /// Read netlist library of cells.
    // TODO: Pass error
    pub fn read_cell_netlist_library(&mut self, path: &str) {
        // Read library.
        info!("Reading netlist library: {}", path);

        // Load interfaces of standard-cells.
        let reader = StructuralVerilogReader::new()
            .load_blackboxes(true); // Skip contents of cells. Only load the interfaces.

        // Load interfaces of standard-cells.
        reader.read_into_netlist(
            &mut BufReader::new(fs::File::open(path).unwrap()),
            &mut self.chip)
            .expect("Failed to read netlist.");

        info!("Number library cells: {}", self.chip.each_cell().count());
    }

    // TODO: Pass error
    pub fn read_design_netlist(&mut self, path: &str) {
        info!("Read top netlist: {}", path);
        let reader = StructuralVerilogReader::new()
            .load_blackboxes(false);

        reader.read_into_netlist(&mut fs::File::open(path).unwrap(),
                                 &mut self.chip).expect("Failed to load netlist.");

        // Print some statistics:
        info!("Number of top-level cells: {}", self.chip.each_top_level_cell().count());
    }

    pub fn select_top_cell(&self, top_cell_name: &str) -> Result<C::CellId, ()> {
        let top = if let Some(top) = self.chip.cell_by_name(top_cell_name) {
            top
        } else {
            log::error!("Top circuit could not be found: {}", top_cell_name);
            // List all possible choices.
            let top_circuits = self.chip.each_cell()
                // .filter(|c| netlist.num_references(c) == 0) // Get top level cells.
                .filter(|c| self.chip.num_child_instances(c) > 0) // Get top level cells.
                .sorted_by_key(|c| self.chip.num_child_instances(c)) // Sort by size of the circuit.
                .rev()
                .map(|c| format!("'{}'", self.chip.cell_name(&c)))
                .join(", ");
            log::error!("Top circuit should be one of: {}", top_circuits);
            return Err(());
        };

        info!("Top level cell: {}", self.chip.cell_name(&top));

        Ok(top)
    }

    /// Deep-flatten the top with pads such that only leaf cells are inside the top cell.
    pub fn deep_flatten_design(&mut self, top: &C::CellId) {
        log::info!("Flatten the top such that it contains only leaf cells.");
        // let top = self.top_cell.clone().expect("No top cell selected.");
        loop {
            let instances = self.chip.each_cell_instance_vec(&top);
            let mut num_flattened_cells = 0;
            for inst in instances {
                let template = self.chip.template_cell(&inst); // Find the cell of this instance.
                let num_sub_instances = self.chip.num_child_instances(&template);
                if num_sub_instances > 0 {
                    // Flatten all non-leaf cells.
                    self.chip.flatten_circuit_instance(&inst);
                    num_flattened_cells += 1;
                }
            }
            if num_flattened_cells == 0 {
                // Nothing more to flatten.
                break;
            }
        }
    }

    /// Sanity check on netlist: Check that all nets in the top cell have a driver.
    pub fn validate_netlist_check_drivers(&self) {
        { // Do sanity checks on the netlist.
            let validation_result = netlist_validation::check_drivers_in_cell(&self.chip, &self.top_cell.as_ref().expect("No top-cell defined."));
            if validation_result.is_err() {
                log::warn!("Some problems with the netlist such as missing tie-cells are about to be resolved.");
            }
        }
    }

    pub fn get_pad_positions(&self,
                             pad_instances: &Vec<C::CellInstId>,
                             (core_width, core_height): (db::Coord, db::Coord))
                             -> HashMap<C::CellInstId, db::Point<C::Coord>> {
        // Create initial positions for the pads.

        let num_pads = pad_instances.len();
        let num_pads_per_side = ((num_pads + 3) / 4) as i32;

        // Distance from the pad ring to the core.
        let margin = 10000;

        let w = core_width + 2 * margin;
        let h = core_height + 2 * margin;

        // Distribute pad positions evenly around the rectangular core.
        let positions_a = (0..num_pads_per_side)
            .map(|i| db::Point::new(-margin, -margin) + db::Vector::new(w, 0) * i / num_pads_per_side);
        let positions_b = (0..num_pads_per_side)
            .map(|i| db::Point::new(core_width + margin, -margin) + db::Vector::new(0, h) * i / num_pads_per_side);
        let positions_c = (0..num_pads_per_side)
            .map(|i| db::Point::new(core_width + margin, core_height + margin) + db::Vector::new(-w, 0) * i / num_pads_per_side);
        let positions_d = (0..num_pads_per_side)
            .map(|i| db::Point::new(0 - margin, core_height + margin) + db::Vector::new(0, -h) * i / num_pads_per_side);

        let positions = positions_a
            .chain(positions_b)
            .chain(positions_c)
            .chain(positions_d);

        let positions: HashMap<_, _> = pad_instances.iter()
            .cloned()
            .zip(positions).collect();
        positions
    }

    /// Get names of cells types used in the top cell.
    pub fn get_used_std_cell_names(&self) -> Vec<String> {
        if let Some(top) = &self.top_cell {
            self.chip.each_cell_dependency(top)
                .map(|c| self.chip.cell_name(&c).to_string()).collect()
        } else {
            vec![]
        }
    }

    pub fn get_layer_name_mapping(&self) -> HashMap<String, (UInt, UInt)> {
        // Define layer names and the mapping to layer indices.
        let layer_name2index: HashMap<String, (UInt, UInt)> = vec![
            // ("poly".into(), (15, 0)),
            // ("contact".into(), (16, 0)),
            ("metal1".into(), (21, 0)),
            ("via1".into(), (22, 0)),
            ("metal2".into(), (23, 0)),
            ("via2".into(), (24, 0)),
            ("metal3".into(), (25, 0)),
            ("via3".into(), (26, 0)),
            ("metal4".into(), (27, 0)),
            ("via4".into(), (28, 0)),
            ("metal5".into(), (29, 0)),
        ].into_iter().collect();
        layer_name2index
    }

    /// Create all named layers and set their names.
    fn initialize_layers(&mut self) {
        for (name, (index, datatype)) in self.get_layer_name_mapping() {
            let l = self.chip.find_or_create_layer(index, datatype);
            self.chip.set_layer_name(&l, Some(name.into()));
        }
    }

    /// Manually add the layout of the dummy pad.
    pub fn load_pad_cell_layouts(&self, layout_library: &mut C) {
        log::info!("Manually create the layout of a dummy pad.");
        let pin_shape_layer = layout_library.find_or_create_layer(21, 0);
        let pin_label_layer = layout_library.find_or_create_layer(21, 1);

        let outline_layer = self.outline_layer.as_ref().expect("outline_layer is not specified.");

        for (pad_name, pin_name) in vec![("dummy_pad_input", "Out"), ("dummy_pad_output", "In")] {
            let dummy_pad_cell = layout_library.create_cell(pad_name.to_string().into());

            // Insert pin shape.
            let geometry: db::Geometry<_> = db::Rect::new((-1000, -1000), (1000, 1000)).into();
            layout_library.insert_shape(&dummy_pad_cell,
                                        &pin_shape_layer,
                                        geometry.clone(),
            );

            // Insert pin label.
            layout_library.insert_shape(&dummy_pad_cell,
                                        &pin_label_layer,
                                        db::Text::new(pin_name.to_string(), (0, 0).into()).into(),
            );

            // Insert pin outline.
            dbg!(outline_layer);
            layout_library.insert_shape(&dummy_pad_cell,
                                        outline_layer,
                                        geometry,
            );
        }
    }

    /// Import all OASIS layout files from a directory.
    /// Put the cell layouts in to the current design.
    pub fn load_cell_layout_library(&mut self, cells_layout_path: &str) {
        // Load cell layouts.
        let mut layout_library = C::new();

        let oasis_reader = libreda_oasis::OASISStreamReader::new();

        info!("Load cell layouts: {}", cells_layout_path);

        // Read all cells found in the directory.
        let dir_content = fs::read_dir(cells_layout_path).unwrap();

        for entry in dir_content {
            if let Ok(entry) = entry {
                let oas_path = entry.path();

                // Read only files with an '.oas' extension.
                let extension = OsStr::new("oas");
                if oas_path.extension() == Some(extension) {
                    log::debug!("Read: {}", oas_path.to_string_lossy());
                    oasis_reader.read_layout(&mut fs::File::open(oas_path.into_boxed_path()).unwrap(),
                                             &mut layout_library)
                        .expect("Failed to read cell layout.");
                }
            }
        }

        self.load_pad_cell_layouts(&mut layout_library);

        // Print some statistics.
        info!("Number of library cell layouts: {}", layout_library.num_cells());

        {
            // Check that all necessary cell layouts are present.
            // TODO: Also take into account cells that are inserted during place & route (buffers, tie-cells, etc.).
            let missing_layouts: Vec<_> = self.get_used_std_cell_names().iter()
                .filter(|&circuit_name| layout_library.cell_by_name(circuit_name).is_none())
                .cloned()
                .collect();
            if missing_layouts.is_empty() {
                info!("All necessary layout cells are present.")
            } else {
                error!("Missing cell layouts: {}", missing_layouts.join(", "));
            }
            assert!(missing_layouts.is_empty(), "Some cell layouts are missing: {}", missing_layouts.join(", "));
        }

        // Copy the shapes of each library cell into the design.
        for source_cell in layout_library.each_cell() {
            let cell_name = layout_library.cell_name(&source_cell);
            let target_cell = self.chip.cell_by_name(cell_name.borrow());
            // Copy shapes only if a cell with the same name exists in the design.
            if let Some(target_cell) = target_cell {
                log::debug!("Copy shapes of cell '{}'", &cell_name);
                db::util::copy_shapes_all_layers(
                    &mut self.chip, &target_cell,
                    &layout_library, &source_cell,
                );
            }
        }
    }

    /// Get the geometric shape of the core.
    pub fn core_area(&self) -> db::SimplePolygon<C::Coord> {
        self.core_area.clone()
            .expect("No core shape defined.")
    }

    /// Extract pin locations from the cell layouts.
    pub fn detect_pins_in_layout(&mut self) {
        info!("Extract pin locations from the cell layouts.");

        let pin_shape_and_label_layers = vec![
            ((21, 0), (21, 1)), // metal1
            ((23, 0), (23, 1))  // metal2
        ];

        for cell in self.chip.each_cell_vec() {
            for (pin_shape_layer, pin_label_layer) in &pin_shape_and_label_layers {
                let pin_shape_layer = self.chip.find_or_create_layer(pin_shape_layer.0, pin_shape_layer.1);
                let pin_label_layer = self.chip.find_or_create_layer(pin_label_layer.0, pin_label_layer.1);

                libreda_pnr::util::pin_detection::assign_pin_shapes_from_text_labels(
                    &mut self.chip,
                    &cell,
                    &pin_shape_layer,
                    &pin_label_layer,
                );
            }
        }
    }

    /// Get the dimensions of the library cells (size of abutment box).
    pub fn get_cell_outlines(&self) -> HashMap<C::CellId, db::Rect<C::Coord>> {

        // Store cells that don't have an abutment box (outline) defined to later display a warning.
        let mut cells_without_bounding_box = BTreeSet::new();

        let outline_layer = self.outline_layer.as_ref().expect("No outline layer defined.");

        // Find the size of the abutment box for every used cell.
        let cell_dimensions: HashMap<_, db::Rect<db::Coord>> = self.chip.each_cell()
            .filter_map(|cell| {
                // Find bounding box of 'outline' layer.

                let bbox = self.chip.bounding_box_per_layer(&cell, &outline_layer);

                if let Some(bbox) = bbox {
                    Some((cell, bbox))
                } else {
                    let name = self.chip.cell_name(&cell);
                    cells_without_bounding_box.insert(name);
                    None
                }
            })
            .collect();

        if !cells_without_bounding_box.is_empty() {
            let outline_layer = self.chip.layer_info(&outline_layer);
            let outline_layer = (outline_layer.index, outline_layer.datatype);
            warn!("Some cells have no abutment box (layer {:?}): {}", outline_layer, cells_without_bounding_box.iter().join(", "));
        }

        cell_dimensions
    }

    /// Check if the design can actually be placed in the given core area.
    pub fn validate_layout_density(&self) -> bool {
        // Compute total needed area of the cells.
        let top = self.top_cell.as_ref().expect("No top cell defined.");
        let cell_dimensions = self.get_cell_outlines();
        let total_cell_area: f64 = self.chip.each_cell_instance(top)
            .map(|inst| {
                let template = self.chip.template_cell(&inst);
                cell_dimensions.get(&template)
                    .map(|r| {
                        let r: Rect<f64> = r.cast();
                        r.height() * r.width()
                    })
                    .unwrap_or(0.)
            }).sum();
        // Estimate resulting placement density.
        let core_shape_float: db::SimplePolygon<f64> = self.core_area().cast();

        let density = {
            let area2: f64 = core_shape_float.area_doubled_oriented();
            total_cell_area / (area2 / 2.)
        };

        info!("Estimated core density: {:0.4}", density);
        if density > 1.0 {
            error!("Density is larger than 1 ({:0.4}).", density);
            return false;
        }
        return true;
    }

    /// Create the power grid.
    pub fn draw_power_grid(&mut self, core_area: &db::SimplePolygon<db::Coord>, row_height: db::Coord) {
        let power_router = PowerRouter {
            stripe_definition: vec![
                PowerLayer {
                    layer: (21, 0),
                    orientation: StripeOrientation::Horizontal,
                    offset_gnd: 0,
                    offset_pwr: row_height,
                    stripe_width: 130,
                    step: row_height * 2,
                }.into(),
                PowerVia {
                    layer: (22, 0),
                    width: 40,
                    via_spacing: 40,
                    enclosure_lower: 10,
                    enclosure_upper: 10,
                }.into(),
                PowerLayer {
                    layer: (23, 0),
                    orientation: StripeOrientation::Horizontal,
                    offset_gnd: 0,
                    offset_pwr: row_height,
                    stripe_width: 200,
                    step: row_height * 2,
                }.into(),
                PowerVia {
                    layer: (24, 0),
                    width: 40,
                    via_spacing: 40,
                    enclosure_lower: 10,
                    enclosure_upper: 10,
                }.into(),
                PowerLayer {
                    layer: (25, 0),
                    orientation: StripeOrientation::Vertical,
                    offset_gnd: 0,
                    offset_pwr: 800,
                    stripe_width: 400,
                    step: 10000,
                }.into(),
                PowerVia {
                    layer: (26, 0),
                    width: 40,
                    via_spacing: 40,
                    enclosure_lower: 10,
                    enclosure_upper: 10,
                }.into(),
                PowerLayer {
                    layer: (27, 0),
                    orientation: StripeOrientation::Horizontal,
                    offset_gnd: 0,
                    offset_pwr: 800,
                    stripe_width: 400,
                    step: 10000,
                }.into()
            ]
        };

        // Create the power grid.
        power_router.draw_power_grid(
            &mut self.chip,
            self.top_cell.as_ref().expect("No top cell defined."),
            core_area,
        );
    }

    /// # Parameters
    /// * `is_initial_placement`: When `true` a quadratic placer will be used as to find the initial placement.
    /// When `false`, the current placement will be used as initial value for ePlace.
    pub fn place_global(&mut self,
                        top_cell: &C::CellId,
                        fixed_instances: &HashSet<C::CellInstId>,
                        is_initial_placement: bool) {
        // ** GLOBAL PLACEMENT **

        // Create placement engine.
        let placer = {
            let target_density = 0.5;
            // Create quadratic placer for initial placement.
            let quadratic_placer = SimpleQuadraticPlacer::new()
                .max_iter(200);

            // Create ePlace engine which relies on the result of the initial placement of the quadratic placer.
            let eplace = {
                let mut eplace = EPlaceMS::new(target_density);
                eplace.max_iter(300);
                eplace
            };

            // Put the placers into series.
            let mut placers: Vec<Box<dyn MixedSizePlacer<C>>> = vec![];
            if is_initial_placement {
                placers.push(Box::new(PlacerAdapter::new(quadratic_placer)));
            }
            placers.push(Box::new(eplace));
            PlacerCascade::new(placers)
        };

        // Region where cells can be placed.
        let core_area = self.core_area();
        let core_shape = db::SimpleRPolygon::try_new(
            core_area.iter().collect()
        ).expect("Failed to convert core shape into a rectilinear polygon.");

        let mut net_weights = HashMap::new();

        // Ignore clock and reset nets for the global placement.
        for clock_name in &self.clock_nets {
            // Find net or net of a pin by name.
            let name = clock_name.as_str();
            let net = self.chip.net_by_name(&top_cell, name)
                .or(self.chip.pin_by_name(&top_cell, name).and_then(|pin|
                    self.chip.net_of_pin(&pin)));

            if let Some(net) = net {
                log::info!("Ignore clock net for global placement: {:?}", self.chip.net_name(&net));
                net_weights.insert(net, 0.);
            } else {
                log::warn!("Clock net or pin not found: {}", clock_name);
            }
        }
        for reset_name in &self.reset_nets {
            // Find net or net of a pin by name.
            let name = reset_name.as_str();
            let net = self.chip.net_by_name(&top_cell, name)
                .or(self.chip.pin_by_name(&top_cell, name).and_then(|pin|
                    self.chip.net_of_pin(&pin)));

            if let Some(net) = net {
                log::info!("Ignore reset net for global placement: {:?}", self.chip.net_name(&net));
                net_weights.insert(net, 0.);
            } else {
                log::warn!("Reset net or pin not found: {}", reset_name);
            }
        }

        // Initialize placement status flags.
        let mut placement_status = HashMap::new();
        for cell_inst in self.chip.each_cell_instance(&top_cell) {
            let status = if fixed_instances.contains(&cell_inst) {
                PlacementStatus::Fixed
            } else {
                PlacementStatus::Movable
            };
            placement_status.insert(cell_inst, status);
        }

        // Assemble the placement problem.
        let design = SimpleDesignRef {
            fused_layout_netlist: &self.chip,
            top_cell: top_cell.clone(),
            cell_outlines: &self.get_cell_outlines(),
            placement_region: &vec![core_shape.clone()],
            placement_status: &placement_status,
            net_weights: &net_weights,
            placement_location: &Default::default(),
        };

        // Do placement.
        let positions = placer.find_cell_positions_impl(
            &design
        );

        match positions {
            Ok(positions) => {
                // Update cell positions.
                for (cell_inst, pos) in positions {
                    self.chip.set_transform(&cell_inst, pos);
                }
            }
            Err(_err) => {
                log::error!("Global placement failed.");
                exit(1);
            }
        }

        {
            // Print HPLW.
            let hplw: f64 = libreda_pnr::metrics::wirelength_estimation::hpwl(&self.top_cell_ref());
            let hplw_microns = hplw / (self.chip.dbu() as f64);
            log::info!("HPLW: {:0.1} um", hplw_microns);
        }
    }

    pub fn insert_tie_cells(&mut self) {
        log::info!("Insert tie-cells.");

        let tie_cell_generator = libreda_pnr::util::tie_cell_insertion::SimpleTieCellInsertionEngine {
            tie_cell_low: "TIELOW".to_string(),
            tie_cell_high: "TIEHIGH".to_string(),
            max_fanout: 10,
            max_distance: 1000,
        };

        let top = self.top_cell.as_ref().expect("No top cell defined.");
        for net in vec![self.chip.net_zero(top), self.chip.net_one(top)] {
            log::info!("Add tie-cells for net {:?}. ({} sinks)", self.chip.net_name(&net), self.chip.num_net_terminals(&net));
            tie_cell_generator.insert_tie_cells_on_net(&mut self.chip, &net).expect("Tie-cell insertion failed.");
        }
    }

    /// Insert buffer-trees on high-fanout nets.
    /// Return the created nets.
    pub fn rebuffer_high_fanout_nets(&mut self) -> Vec<C::NetId> {
        // Find high-fanout nets and insert buffer trees.
        log::info!("Insert buffer trees on high-fanout nets.");
        let top = self.top_cell.clone().expect("No top cell defined");


        let max_fanout = 4; // TODO: Centralize configuration.

        let high_fanout_nets: Vec<_> = self.chip.each_internal_net(&top)
            .filter(|n| self.chip.num_net_terminals(n) > max_fanout)
            .collect();

        log::info!("Number of high-fanout nets in '{}' (with more than {} terminals): {}",
                   self.chip.cell_name(&top),
                   max_fanout, high_fanout_nets.len());
        log::info!("Insert buffers into high-fanout nets.");

        let buffer_generator = arboreus_cts::simple_buffer_tree::SimpleBufferInsertionEngine {
            inverting_buffer_cell: "INVX1".into(),
            max_fanout: max_fanout as u32,
        };

        let mut buffer_tree_nets = vec![];
        for n in high_fanout_nets {
            log::debug!("Add buffer tree into net {:?}.", self.chip.net_name(&n));
            let (_buffer_insts, nets) = buffer_generator.add_buffer_tree_on_net(&mut self.chip, &n).expect("Buffer insertion failed.");
            buffer_tree_nets.extend(nets);
            buffer_tree_nets.push(n);
        }
        buffer_tree_nets
    }


    pub fn route_with_mycelium(&mut self) {
        let top_cell = self.top_cell.clone().expect("No top cell defined.");

        let wire_width = 50i32;
        let min_spacing = 50;

        // let wire_weights = (0..num_layers)
        //     .map(|i| if i % 2 == 1 {
        //         // Prefer horizontal routes.
        //         (1, 10)
        //     } else {
        //         // Prefer vertical routes.
        //         (10, 1)
        //     })
        //     .collect_vec();
        let wire_weights = vec![
            (100, 100), // metal1
            (2, 20), // metal2
            (20, 2), // metal3
            (2, 20), // metal4
            (20, 2), //metal5
            // (1, 10), // metal6
            // (10, 1), //metal7
        ];

        let routing_layers = self.get_routing_layers();
        assert!(wire_weights.len() <= routing_layers.len(), "Try to use more routing layers than specified in the technology.");
        let via_layers = self.get_via_layers();

        let global_router = SimpleGlobalRouter::new(
            wire_width / 2,
            min_spacing,
            wire_weights.clone(),
            routing_layers.clone(),
            via_layers.clone(),
        );

        // Global routing
        let global_routes = {
            let global_routing_problem = {
                let top_cell = self.top_cell.clone().unwrap();
                let all_nets = self.chip.each_internal_net(&top_cell).collect();

                let mut routing_problem: SimpleRoutingProblem<_, ()> = SimpleRoutingProblem::new(&self.chip, top_cell);
                routing_problem.nets = all_nets;

                routing_problem
            };

            let routes = global_router.route(
                &global_routing_problem,
            );

            match routes {
                Err(unrouted_nets) => {
                    log::error!("Global routing failed. Number of unrouted nets: {}", unrouted_nets.len());
                    todo!("Global routing failed.");
                }
                Ok(routes) => routes
            }
        };

        let _airwire_router = AirWireRouter::new(0, 20, true);

        let simple_maze_router = SimpleMazeRouter::new(
            wire_width / 2,
            min_spacing,
            wire_weights,
            routing_layers.clone(),
            via_layers.clone(),
        );

        // let router = SimpleLineSearchRouter::new(wire_width / 2, min_spacing, wire_weights);

        let routing_result = {
            let mut detail_routing_problem = SimpleRoutingProblem::new(
                &self.chip,
                top_cell.clone(),
            )
                .with_routing_guides(global_routes);

            // Tell the route which nets to route.
            {
                // Since the detail routing is not that advanced yet, skip high-fanout nets.
                let max_fanout = 100;
                log::warn!("Route nets with up to {} terminals.", max_fanout);
                let nets = self.get_low_fanout_nets(max_fanout);
                log::info!("Number of nets selected for routing: {}", nets.len());
                detail_routing_problem.nets.extend(nets);
            }

            simple_maze_router.route(detail_routing_problem)
        };

        if let Err((_result, unrouted_nets)) = &routing_result {
            log::error!("Detail routing failed.");
            log::error!("Number of unrouted nets: {}", unrouted_nets.len())
        } else {
            log::info!("Routed all nets.")
        }

        // Draw the routes to the layout.
        match routing_result {
            Ok(routes) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
            Err((routes, _unrouted_nets)) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
        }

        // let routing_result = router.route_all_nets(
        //     &mut self.chip,
        //     top_cell,
        //     &routing_layers,
        //     &via_layers,
        // );
        //
        // if let Err(unrouted_nets) = &routing_result {
        //     log::error!("Number of unrouted nets: {}", unrouted_nets.len());
        //     log::info!("Draw unrouted nets.");
        //     let airwire_result = airwire_router.route_nets(
        //         &mut self.chip,
        //         top_cell,
        //         &routing_layers,
        //         &via_layers,
        //         unrouted_nets,
        //     );
        //     if airwire_result.is_err() {
        //         // That should not happen.
        //         log::error!("Some unrouted nets could not be displayed with airwires.");
        //     }
        // } else {
        //     log::info!("Routed all nets.")
        // }
    }

    /// Route the design using TritonRoute(-WXL).
    pub fn route_with_triton(&mut self) {
        let top_cell = self.top_cell.clone().expect("No top cell defined.");


        // Set routing tracks.
        let triton = {
            // let mut triton: libreda_triton_route::TritonRoute<Chip> = libreda_triton_route::TritonRoute::new(
            //     "/opt/TritonRoute".into(), technology_lef_path.into(),
            //     self.outline_layer.expect("No outline layer specified."),
            // );

            let mut triton: libreda_triton_route::TritonRoute<C> = libreda_triton_route::TritonRoute::new_wxl(
                "/opt/TritonRoute-WXL".into(), self.tech_lef_path.clone(),
                self.outline_layer.clone().expect("No outline layer specified."),
            );


            // Set routing tracks.
            {
                let die_area: db::Rect<_> = self.core_area().try_bounding_box().expect("Core shape must have a defined bounding box.");
                let def = triton.template_def();

                // Add TRACKS statements to the DEF structure.
                let routing_layers = self.get_routing_layers();
                for routing_layer in &routing_layers {
                    let layer_name: String = self.chip.layer_info(routing_layer).name.unwrap().into();

                    let pitch = 100;

                    let tracks_h = Tracks {
                        is_horizontal: true,
                        start: die_area.lower_left().y,
                        num_tracks: (die_area.height() / pitch + 1) as u32,
                        step: pitch,
                        mask: None,
                        layers: vec![layer_name],
                    };

                    def.tracks.push(tracks_h.clone());

                    let tracks_v = Tracks {
                        is_horizontal: false,
                        start: die_area.lower_left().x,
                        num_tracks: (die_area.width() / pitch + 1) as u32,
                        ..tracks_h
                    };

                    def.tracks.push(tracks_v);
                }
            }

            triton
        };

        if !triton.check_configuration() {
            let msg = "Something is wrong with the configuration of TritonRouter.";
            log::error!("{}", msg);
            panic!("{}", msg);
        }


        // Detail routing.
        {
            // FIXME: Hack remove pad cells because they have no defined outline and are not specified in the LEF file.
            self.chip.remove_cell(&self.chip.cell_by_name("dummy_pad_output").unwrap());
            self.chip.remove_cell(&self.chip.cell_by_name("dummy_pad_input").unwrap());
            self.chip.remove_cell(&self.chip.cell_by_name("TIEHIGH").unwrap());
            self.chip.remove_cell(&self.chip.cell_by_name("TIELOW").unwrap());
            //
            // Remove all the nets which are not used after removing the above cells.
            self.chip.purge_nets_in_circuit(&top_cell);
        }

        // Make sure all nets have names. This is required for DEF export and import.
        self.chip.create_net_names_in_circuit(&top_cell, "_");

        let routing_result = {
            let mut detail_routing_problem: SimpleRoutingProblem<_, SimpleGlobalRoute<C>> = SimpleRoutingProblem::new(
                &self.chip,
                top_cell.clone(),
            );

            // Tell the route which nets to route. (All nets in this case).
            detail_routing_problem.nets.extend(self.chip.each_internal_net(&top_cell));

            triton.route(detail_routing_problem)
        };

        if let Err((_result, unrouted_nets)) = &routing_result {
            log::warn!("Detail routing did not complete for all nets (mainly because detail routing is not fully implemented yet).");
            log::warn!("Number of unrouted nets: {}", unrouted_nets.len())
        } else {
            log::info!("Routed all nets.")
        }

        // Draw the routes to the layout.
        match routing_result {
            Ok(routes) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
            Err((routes, _unrouted_nets)) => {
                let draw_result = routes.draw_detail_routes(&mut self.chip, &top_cell);
                if draw_result.is_err() {
                    log::error!("Failed to draw routes to layout.");
                }
            }
        }
    }

    /// Store the layout into a OASIS file.
    pub fn store_layout(&self, stream_out_path: &str) {
        info!("Stream out: {}", stream_out_path);
        let oasis_writer = libreda_oasis::OASISStreamWriter::default();
        debug!("Number of cells in layout: {}", self.chip.num_cells());

        // TODO: Already open the file to detect early if it fails.
        let stream_out_file = &mut fs::File::create(stream_out_path).unwrap();
        let err = oasis_writer.write_layout(&mut BufWriter::new(stream_out_file), &self.chip);
        if let Err(err) = err {
            error!("Error: {}", err);
        }
    }

    /// Dump the netlist to a verilog file.
    pub fn store_netlist(&self, path: &str) {
        let netlist_writer = StructuralVerilogWriter::new();
        log::info!("Write verilog netlist: {}", path);
        let mut netlist_output_file = fs::File::create(path).unwrap();
        netlist_writer.write_netlist(&mut netlist_output_file, &self.chip)
            .expect("Failed to write netlist.");
    }

    /// Get a list of nets in the current top-cell.
    pub fn get_low_fanout_nets(&self, max_fanout: usize) -> Vec<C::NetId> {

        // Sort nets. Nets with highest fanout come first.
        self.top_cell_ref()
            .each_net()
            .filter(|net| net.num_terminals() <= max_fanout)
            .map(|net| net.id())
            .collect()
    }

    /// Print a list of nets with highest number of pins.
    pub fn report_high_fanout_nets(&self, max_num_nets: usize) {
        log::info!("Report high-fanout nets.");
        // Sort nets. Nets with highest fanout come first.
        let mut nets: Vec<_> = self.top_cell_ref().each_net().collect();
        nets.sort_by_key(|net| net.num_terminals());
        nets.reverse();

        // Print the first few nets.
        let nets = nets.into_iter().take(max_num_nets);
        for net in nets {
            println!("{} terminals: {}", net.num_terminals(), net.qname(":"));
        }
    }
}
