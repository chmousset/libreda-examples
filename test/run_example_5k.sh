#!/bin/bash

# Run a simple-stupid example place and route flow.
# Inputs are: Directory to the cell layouts, a verilog file with the cell interfaces
# a LEF file with the cell library, a synthesized gate-level netlist the name of the top-level cell
# and core dimensions.
# 
# Use the --help argument to display documentation on the arguments.
#

ARGS="${@:1}"

mkdir test/output

RUST_LOG=info cargo run \
    --release \
    --bin libreda-example -- \
    --cell-layouts ./data/oas/ \
    --cell-library ./data/gscl45nm_interfaces.v \
    --lef ./data/gscl45nm.lef \
     --netlist ./data/ottocore_freepdk45_nl_flat.v \
     --top ottocore \
    --reset rst_ni \
    --clock clk_i \
    --height 400000 --width 400000 \
    --output ./output/ottocore_5k.oas \
    $ARGS

