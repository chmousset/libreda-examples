#!/bin/bash

# Run a simple-stupid example place and route flow.
# Inputs are: Directory to the cell layouts, a verilog file with the cell interfaces
# a LEF file with the cell library, a synthesized gate-level netlist the name of the top-level cell
# and core dimensions.
# 
# Use the --help argument to display documentation on the arguments.
#

ARGS="${@:1}"

mkdir -p test/output

export RUST_LOG=info

cargo run \
    --bin libreda-example -- \
    --cell-layouts ./data/oas/ \
    --cell-library ./data/gscl45nm_interfaces.v \
    --lef ./data/gscl45nm.lef \
    --netlist ./data/comb_chip_small.v \
    --top my_chip \
    --height 20000 --width 20000 \
    --output ./output/example_output.oas \
    $ARGS
