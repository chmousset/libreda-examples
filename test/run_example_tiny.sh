#!/bin/bash

# Run a simple-stupid example place and route flow.
# Inputs are: Directory to the cell layouts, a verilog file with the cell interfaces
# a LEF file with the cell library, a synthesized gate-level netlist the name of the top-level cell
# and core dimensions.
# 
# Use the --help argument to display documentation on the arguments.
#

ARGS="${@:1}"

mkdir -p test/output

export RUST_LOG=info


echo "This example runs in DEBUG mode (slow)."

cargo run \
    --bin libreda-example -- \
    --cell-layouts ./data/oas/ \
    --cell-library ./data/gscl45nm_interfaces.v \
    --lef ./data/gscl45nm.lef \
    --netlist ./data/comb_chip_45_nl.v \
    --top my_chip \
    --height 40000 --width 40000 \
    --output ./output/example_output.oas \
    $ARGS
